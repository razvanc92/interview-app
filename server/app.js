
var express = require('express');
var app = express();

var bodyParser = require('body-parser')
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: false
})); 

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	
  next();
});

app.post('/public/signup/:pathwayId', function (req, res) {
  	console.log('firstname: ' + req.body.firstname);
	console.log('lastname:  ' + req.body.lastname);
	console.log('email:     ' + req.body.email);
	console.log('pathwayId: ' + req.params.pathwayId);
	if(req.body.firstname === 'error') {
		res.status(400);
		res.send('None shall pass');
	}
	else  {
		res.send({"info":{"request":"/public/signup/5566e151817a62021b1ea809","result":"success","http":200},"data":{"_id":"5569c73d817a62021b1ec0fb"}});
	}
});

var server = app.listen(3000, function () {

  var host = server.address().address;
  var port = server.address().port;

  console.log('MEDEI API simulator listening at http://%s:%s', host, port);

});
