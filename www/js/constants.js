/**
 * Created by Cirstea on 5/29/2015.
 */
angular.module('starter.constants', [])
    .constant('Settings', {
        apiDomain: 'https://test-api.smart-trial.dk/api/',
        registerPath: 'public/signup/',
        pathwayId: '5566e151817a62021b1ea809'
    });
