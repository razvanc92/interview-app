angular.module('starter.services', [])

    .factory('ApiAccess', function ($http, Settings) {
        ApiAccess = {};
        ApiAccess.createAccount = function (accountData) {
            return $http({
                method: 'post',
                url: Settings.apiDomain + Settings.registerPath + Settings.pathwayId,
                data: accountData,
                transformRequest: function (obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                headers: {"Content-Type": 'application/x-www-form-urlencoded', "Referer": ''}
            });
        };
        return ApiAccess;
    });

