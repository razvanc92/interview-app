angular.module('starter.controllers', [])

    .controller('HomeCtrl', function ($scope,$rootScope,$state) {
        $rootScope.go = function(location){
            $state.go(location);
        }
    })

    .controller('AccountCtrl', function ($scope, $http, ApiAccess, $ionicPopup, $state, $rootScope) {
        $scope.formSubmitted = false;
        $scope.accountData = {};
        $scope.settings = {
            enableFriends: true
        };

        $scope.showMessage = function (title, body, callback) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: "<div style='text-align:center'>" + body + "</div>"
            })
            alertPopup.then(function () {
                if (callback) {
                    callback()
                }
            })
        };
        $scope.createAccount = function (accountData) {
            $scope.formSubmitted = true;
            if (accountData.firstname && accountData.lastname && accountData.email) {
                ApiAccess.createAccount(accountData).success(function (res) {
                    $scope.formSubmitted = false;
                    accountData.firstname = "";
                    accountData.lastname = "";
                    accountData.email = "";
                    var id = res.data._id;
                    $scope.showMessage('Success', 'Account created successfully, <br> your uniqe id is:' + id, function () {
                        $rootScope.go('tab.home');
                    });
                }).error(function () {
                    $scope.showMessage('Ooops', 'There has been some kind of error')
                });
            }
        }
    }
);

